﻿using UnityEngine;
using System.Collections;

public class ManoEnemiga : MonoBehaviour {
    public float maxTiempo;
    public Manager manager;
    private bool escapar;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider col) {
        if (col.CompareTag("Player")) {
            StartCoroutine(Peligro());
        }
    }

    void OnTriggerExit(Collider col) {
        if (col.CompareTag("Player")) {
            escapar = true;
        }
    }

    public IEnumerator Peligro() {
        float inicio = Time.time;
        while(!escapar) {
            if(Time.time - inicio > maxTiempo) {
                manager.Perder();
                break;
            }
            yield return new WaitForFixedUpdate();
        }
        escapar = false;
    }

}
