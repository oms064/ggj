﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(AudioSource))]
public class SonidoNPC : MonoBehaviour {
    private AudioSource controlSonidos;
    private bool inRange;
    public AudioClip sonido;
    public float intervalo = 1.5f;

    void Start() {
        controlSonidos = GetComponent<AudioSource>();
        inRange = false;
    }

#if UNITY_EDITOR
    void OnDrawGizmos() {
        Gizmos.DrawIcon(this.transform.position, "enemy.jpg");
    }
#endif

    public void BeginSound() {
        inRange = true;
        StartCoroutine(Reproducir());
    }

    public void EndSound() {
        inRange = false;
    }

    public IEnumerator Reproducir() {
        while (inRange) {
            controlSonidos.PlayOneShot(sonido);
            yield return new WaitForSeconds(intervalo);
        }
    }


}
