﻿using UnityEngine;
using System.Collections;

public class Sonidos : MonoBehaviour {
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {

    }

    void OnTriggerEnter(Collider objeto) {
        if (objeto.CompareTag("NPC"))
            objeto.GetComponent<SonidoNPC>().BeginSound();
    }

    void OnTriggerExit(Collider objeto) {
        if (objeto.CompareTag("NPC"))
            objeto.GetComponent<SonidoNPC>().EndSound();
    }
}
