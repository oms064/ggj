﻿using UnityEngine;
using System.Collections;

public class CreditsRoll : MonoBehaviour {

    float advance = 1f;
	// Use this for initialization
	void Start () {
		print("Starting " + Time.time);
		StartCoroutine(WaitAndPrint(55.0F));
		print("Before WaitAndPrint Finishes " + Time.time);

	}
	
	// Update is called once per frame
	void Update () {

        transform.position = new Vector3(transform.position.x, advance, 0);
        advance+=1f;
    }

	IEnumerator WaitAndPrint(float waitTime) {
		yield return new WaitForSeconds(waitTime);
		print("WaitAndPrint " + Time.time);
		Application.LoadLevel(0);
	}

}
