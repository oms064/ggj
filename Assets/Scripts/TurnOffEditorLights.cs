﻿using UnityEngine;
using System.Collections;

public class TurnOffEditorLights : MonoBehaviour {

	// Use this for initialization
	void Start () {
        this.GetComponent<Light>().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
