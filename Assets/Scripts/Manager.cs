﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour {
    public Canvas menuPausa;
    public bool pausa, anterior;
    public GameObject player, enemigo;
    private Animator animEnemigo;
	// Use this for initialization
	void Start () {
        menuPausa.enabled = false;
        Time.timeScale = 1.0f;
	}

    void Update() {
        if (pausa && !anterior) {
            StartCoroutine(Pausar());
        }
        anterior = pausa;
        if (Input.GetKeyDown(KeyCode.P)) {
            pausa = !pausa;
        }
    }

    private IEnumerator Pausar() {
        Time.timeScale = 0;
        player.GetComponent<FirstPersonController>().enabled = false;
        menuPausa.enabled = true;
        while (pausa) {
            yield return null;
        }
        menuPausa.enabled = false;
        player.GetComponent<FirstPersonController>().enabled = true ;
        Time.timeScale = 1;
    }

    public void setPausa(bool p) {
        pausa = p;
    }

    public void Perder() {
        //Aquí va el código de la animación de GameOver
        
        //SceneManager.LoadScene();
    }

    public void Ganar() {
        //Aquí va alguna animación de victoria?
        SceneManager.LoadScene(2);
    }
}
