﻿using UnityEngine;
using System.Collections;

public class Hide_menu_keys : MonoBehaviour {
	void Start() {
		print("Starting " + Time.time);
		StartCoroutine(WaitAndPrint(5.0F));
		print("Before WaitAndPrint Finishes " + Time.time);
	}
	IEnumerator WaitAndPrint(float waitTime) {
		yield return new WaitForSeconds(waitTime);
		print("WaitAndPrint " + Time.time);
		this.gameObject.SetActive (false);
	}
}
