﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityStandardAssets.Characters.FirstPerson;

public class DetectarPasos : MonoBehaviour {
    public Terrain ter;
    private TerrainData terData;
    private Vector3 terPos;
    private float mapX, mapZ;
    private FirstPersonController controller;
	// Use this for initialization
	void Start () {
        controller = this.GetComponent<FirstPersonController>();
    }
	
	// Update is called once per frame
	void Update () {
        terData = ter.terrainData;
        terPos = ter.transform.position;
        mapX = ((this.transform.position.x - terPos.x) / terData.size.x) * terData.alphamapWidth;
        mapZ = ((this.transform.position.z - terPos.z) / terData.size.z) * terData.alphamapHeight;
        var map = terData.GetAlphamaps((int)mapX, (int)mapZ, 1, 1);
        float[] splats = new float[] { map[0, 0, 0], map[0, 0, 1], map[0, 0, 2] };
        int mayor = 0;
        int posMayor = 0;
        for(int i = 0; i < 3; i++) {
            if(splats[i] > mayor) {
                posMayor = i;
            }
        }
        controller.cambiarPisadas(posMayor);
    }
}

