﻿using UnityEngine;
using System.Collections;

public class Boton_cambiar_Escena : MonoBehaviour {

	public AudioSource audio;
	public int sceneNum;
	bool cond = true;
	bool fadeOutBool = false;

	// Use this for initialization
	void Start () {
		print("Starting " + Time.time);
		StartCoroutine(WaitAndPrint(2.0F));
		print("Before WaitAndPrint Finishes " + Time.time);

	}

	IEnumerator WaitAndPrint(float waitTime) {
		int count = 0;
		while (cond) {
			yield return new WaitForSeconds(0.1F);
//			print("WaitAndPrint " + count);
			if (fadeOutBool) {
				audio.volume = audio.volume - 0.007F;
			}
			if (audio.volume < 0.1F) {
				cond = false;
			}
		}
		Application.LoadLevel(sceneNum);
	}
	// Update is called once per frame
	void Update () {
	
	}

	public void changeScene(){
		print ("mouse");
		fadeOutBool = true;
		
	}

	public void exitGame(){
		Application.Quit();
	}

	void fadeOutMusica(){

	}
}
