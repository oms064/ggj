﻿using UnityEngine;
using System.Collections;
using System;

public class SphereCoordinate : PolarCoordinate{

    public SphereCoordinate (Vector3 vector) {
		magnitude = vector.magnitude;
		if (vector.x == 0) {
			if (vector.z == 0f) {
				longitude = 0f;	
			}else if (vector.z > 0f) {
				longitude = 90f;
			}else {
				longitude = -90f;
			}
		}else {
			longitude = Mathf.Atan(vector.z / vector.x) * Mathf.Rad2Deg;	
		}
		latitude = Mathf.Asin(vector.y / magnitude) * Mathf.Rad2Deg;
		if (vector.x < 0f && vector.z < 0f ) {
			longitude += 270f;
		}else if (vector.x < 0f) {
			longitude += 180f;
		}
		
	}
	
	public SphereCoordinate () {
		latitude = 0;
		longitude = 0;
		magnitude = 0;
	}

	public override void UpdateVar(){
		if (longitude > 360f) {
			longitude -= 360f;
		}else if (longitude < 0f) {
			longitude += 360f;
		}
	}

	public override void UpdateCoord(Vector3 vector){
		magnitude = vector.magnitude;
		if (vector.x == 0) {
			if (vector.z == 0f) {
				longitude = 0f;	
			}else if (vector.z > 0f) {
				longitude = 90f;
			}else {
				longitude = -90f;
			}
		}else {
			longitude = Mathf.Atan(vector.z / vector.x) * Mathf.Rad2Deg;	
		}
		latitude = Mathf.Asin(vector.y / magnitude) * Mathf.Rad2Deg;
		if (vector.x < 0f && vector.z < 0f ) {
			longitude += 270f;
		}else if (vector.x < 0f) {
			longitude += 180f;
		}	
	}

	public override Vector3 ToVector3(){
		float ro = magnitude *Mathf.Cos(latitude * Mathf.Deg2Rad); 
		float x = ro * Mathf.Cos(longitude * Mathf.Deg2Rad);
		float y = magnitude * Mathf.Sin(latitude * Mathf.Deg2Rad);
		float z = ro * Mathf.Sin(longitude * Mathf.Deg2Rad);
		return new Vector3(x, y, z);
	}

    public override Vector3 ToVector3(float y) {
        throw new NotImplementedException();
    }
    public override void UpdateCoord(Vector3 vector, Vector3 centro) {
        throw new NotImplementedException();
    }
}
