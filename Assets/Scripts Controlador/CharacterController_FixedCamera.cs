﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Rigidbody))]

///<summary>
/// Esta clase hace que nuestro personaje gire alrededor de su padre y la cámara siempre esté fija en él.
/// El GameObject que contenga este script deberá tener afuerzas un padre o de lo contrario marcará error.
/// </summary>
public class CharacterController_FixedCamera : MonoBehaviour {
    private Transform destino;
    [Range(0.0f, 1.0f)] public float velocidad = 0.1f;
    private PolarCoordinate sphCoord;
    public Camera cam;

	// Use this for initialization
	void Start () {
        destino = this.transform.parent;
        sphCoord = new CircCoordinate(this.transform.position, destino.position);
    }
	
	// Update is called once per frame
	void Update () {
        sphCoord.latitude += Input.GetAxis("Horizontal") * velocidad;
        if(sphCoord.magnitude <= 0.0f)
            sphCoord.magnitude += Input.GetAxis("Vertical") * velocidad * 2;
        else
            sphCoord.magnitude -= Input.GetAxis("Vertical") * velocidad * 2;
        cam.transform.LookAt(destino);
    }

    void FixedUpdate() {
        this.transform.localPosition = sphCoord.ToVector3(this.transform.position.y) * Time.deltaTime;
    }
}
