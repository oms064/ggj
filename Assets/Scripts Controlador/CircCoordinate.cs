﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Clase que convierte nuestras coordenadas cartesianas en coordenadas circulares en 3 dimensiones para 
/// poder trabajar con un ángulo y una magnitud en lugar de XYZ.
/// </summary>
public class CircCoordinate : PolarCoordinate {

    /// <summary>
    /// Constructor que recibe un la posición de un Gameobject sobre el cual calculará las coordenadas locales.
    /// </summary>
    /// <param name="vector">Posicion del Gameobject</param>
    /// <param name="centro">Posición sobre la que girará nuestro GameObject</param>
    public CircCoordinate(Vector3 vector, Vector3 centro) {
        center = centro;
        magnitude = Mathf.Sqrt(Mathf.Pow(vector.x - center.x, 2.0f) + Mathf.Pow(vector.z - center.z, 2.0f));
        latitude = Mathf.Asin((vector.z - center.z) / magnitude);
        longitude = 0.0f;
    }
    /// <summary>
    /// Constructor que recibe la posición de un Gameobject sobre la cuál calculará sus coordenadas.
    /// </summary>
    /// <param name="vector">La posición de nuestro Gameobject</param>
    public CircCoordinate(Vector3 vector) {
        magnitude = Mathf.Sqrt(Mathf.Pow(vector.x, 2.0f) + Mathf.Pow(vector.z, 2.0f));
        latitude = Mathf.Asin(vector.x / magnitude);
        longitude = 0.0f;
        center = new Vector3();
    }

    public CircCoordinate() {
        magnitude = 0.0f;
        latitude = 0.0f;
        longitude = 0.0f;
    }

    /// <summary>
    /// Convierte nuestras coordenadas circulares en X, 0.0f, Z
    /// </summary>
    /// <returns></returns>
    public override Vector3 ToVector3() {
        return new Vector3(magnitude * Mathf.Cos(latitude) * Mathf.Rad2Deg, 0.0f, magnitude * Mathf.Sin(latitude) * Mathf.Rad2Deg);
    }

    /// <summary>
    /// Convierte nuestras coordenadas circulares en XYZ
    /// </summary>
    /// <param name="y">Valor de Y, regresa el mismo valor</param>
    /// <returns></returns>
    public override Vector3 ToVector3(float y) {
        return new Vector3(magnitude * Mathf.Cos(latitude) * Mathf.Rad2Deg, y, magnitude * Mathf.Sin(latitude) * Mathf.Rad2Deg);
    }

    public override void UpdateVar() {
        if (latitude < 0.0f)
            latitude += 360.0f;
        else if (latitude > 360.0f)
            latitude -= 360.0f;
    }

    public override void UpdateCoord(Vector3 vector) {
        magnitude = Mathf.Sqrt(Mathf.Pow(vector.x - center.x, 2.0f) + Mathf.Pow(vector.z - center.z, 2.0f));
        latitude = Mathf.Asin((vector.x - center.x) / magnitude);
    }

    public override void UpdateCoord(Vector3 vector, Vector3 centro) {
        center = centro;
        magnitude = Mathf.Sqrt(Mathf.Pow(vector.x - center.x, 2.0f) + Mathf.Pow(vector.z - center.z, 2.0f));
        latitude = Mathf.Asin((vector.x - center.x) / magnitude);
    }
}
