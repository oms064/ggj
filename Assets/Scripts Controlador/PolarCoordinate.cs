﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Clase base para algún sistema de coordenadas polar.
/// </summary>
public abstract class PolarCoordinate {
    public float longitude;
    public float latitude;
    public float magnitude;
    public Vector3 center;
    public abstract Vector3 ToVector3();
    public abstract Vector3 ToVector3(float y);
    public abstract void UpdateVar();
    public abstract void UpdateCoord(Vector3 vector);
    public abstract void UpdateCoord(Vector3 vector, Vector3 centro);
}
