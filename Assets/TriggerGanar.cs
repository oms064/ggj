﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class TriggerGanar : MonoBehaviour {

	public FirstPersonController player;
	public AudioSource audioFinalTumba;
	bool cond = true;
	bool fadeOutBool = false;

	void Start () {
		print("Starting " + Time.time);
		StartCoroutine(WaitAndPrint());
		print("Before WaitAndPrint Finishes " + Time.time);

	}

	IEnumerator WaitAndPrint() {
		int count = 0;
		while (cond) {
			yield return new WaitForSeconds(0.1F);
			print("Ciclo " + count);
			print("Volumen" + audioFinalTumba.volume );

			if (fadeOutBool) {
				audioFinalTumba.volume = audioFinalTumba.volume - 0.002F;
			}
			if (audioFinalTumba.volume < 0.01F) {
				cond = false;
			}
			count++;
		}
		Application.LoadLevel(2);
	}

	void OnTriggerEnter(Collider other) {
		print("Entro tumba");
		fadeOutBool = true;
		player.m_WalkSpeed = 0.0F;
		// DESACTIVAR CONTROLES

//		Destroy(other.gameObject);
	}
}
